package br.com.gonleo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {



    public static void main(String[] args) throws IOException {

        AnalisadorSintatico sintatico = new AnalisadorSintatico();
        List<Token> tokens = new ArrayList<>();

        AnalisadorLexico lex = new AnalisadorLexico();
        Token token;
        do {
            token =  lex.buscaToken();
            tokens.add(token);
            System.out.println(token);
        } while(token != null);
        tokens.remove(tokens.size() - 1);
        tokens.remove(tokens.size() - 1);
        sintatico.analitico(tokens);

    }
}
