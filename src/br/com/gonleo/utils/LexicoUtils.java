package br.com.gonleo.utils;

public class LexicoUtils {

    public static boolean isCalculo(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
    }

    public static boolean isParametro(char c) {
        return c == '[' || c == ']' || c == '(' || c == ')';
    }

    public static boolean isEspaco(char c) {
        return (c == ' ' || c == '\n' || c == '\r' || c == '\t');
    }

    public static boolean isNumero(char c) {
        return c >= '0' && c <= '9';
    }

    public static boolean isLetra(char charCompare, char charParam) {
        return Character.toUpperCase(charCompare) == charParam;
    }

    public static boolean isPonto(char c) {
        return c == '.';
    }

}
