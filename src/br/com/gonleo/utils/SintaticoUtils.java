package br.com.gonleo.utils;

import br.com.gonleo.Token;

import java.util.List;

public class SintaticoUtils {
    public static boolean trataSoma(List<String> simbolos, List<Integer> estados, List<Token> entrada){
        switch (estados.get(estados.size() - 1)) {
            case 1:

            case 14:
                estados.add(8);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;
            case 5:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;
            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;
            case 15:
            case 16:

            case 17:

            case 18:

            case 19:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                estados.remove(estados.size() - 1);
                return true;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 22:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;

        }

        return false;
    }

    public static boolean trataSub(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 1:

            case 14:
                estados.add(9);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

            case 5:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;

            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 15:
            case 16:
            case 17:

            case 18:

            case 19:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                estados.remove(estados.size() - 1);
                return true;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 22:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                return true;
        }
        return false;
    }

    public static boolean trataMult(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 2:

            case 15:

            case 16:
                estados.add(10);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

            case 5:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;

            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 17:

            case 18:


            case 19:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                estados.remove(estados.size() - 1);
                return true;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 22:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                return true;

        }
        return false;
    }

    public static boolean trataDiv(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 2:

            case 15:

            case 16:
                estados.add(11);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

            case 5:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;

            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 17:

            case 18:


            case 19:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                estados.remove(estados.size() - 1);
                return true;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 22:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                return true;

        }
        return false;
    }

    public static boolean trataPow(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 3:
                estados.add(12);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

            case 5:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;

            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 17:

            case 18:
                estados.add(11);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

            case 19:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                estados.remove(estados.size() - 1);
                return true;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 22:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                return true;

        }
        return false;
    }

    public static boolean trataExp(List<String> simbolos, List<Integer> estados, List<Token> entrada) {

        switch (estados.get(estados.get(estados.size() - 1))) {
            case 0:

            case 6:

            case 8:

            case 9:

            case 10:

            case 11:
                estados.add(4);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

        }
        return false;
    }

    public static boolean trataColOpen(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        if (estados.get(estados.get(estados.size() - 1)).equals(4)) {
            estados.add(13);
            simbolos.add(entrada.get(entrada.size() - 1).getTermo());
            entrada.remove(entrada.size() - 1);
        }
        return false;
    }

    public static boolean trataColClose(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 20:
                estados.add(22);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

        }
        return false;
    }

    public static boolean trataParOpen(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 0:

            case 8:

            case 9:

            case 10:

            case 11:

            case 12:

            case 13:
                estados.add(6);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

        }
        return false;
    }

    public static boolean trataParClose(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 5:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;

            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 14:
                estados.add(21);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;

            case 15:
            case 16:

            case 17:

            case 18:

            case 19:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                estados.remove(estados.size() - 1);
                return true;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 22:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                return true;
        }
        return false;
    }

    public static boolean trataId(List<String> simbolos, List<Integer> estados, List<Token> entrada) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 0:
            case 6:

            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                estados.add(7);
                simbolos.add(entrada.get(entrada.size() - 1).getTermo());
                entrada.remove(entrada.size() - 1);
                break;
        }
        return false;
    }

    public static boolean trataCifrao(List<String> simbolos, List<Integer> estados) {
        switch (estados.get(estados.get(estados.size() - 1))) {
            case 5:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                break;

            case 7:
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 15:
            case 16:

            case 17:

            case 18:

            case 19:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                estados.remove(estados.size() - 1);
                return true;

            case 21:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("F");
                estados.remove(estados.size() - 1);
                return true;

            case 22:
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.remove(simbolos.size() - 1);
                simbolos.add("P");
                estados.remove(estados.size() - 1);
                return true;
        }
        return false;
    }

    public static void trataE(List<Integer> estados) {
        switch (estados.get(estados.size() - 1)) {
            case 0:
                estados.add(1);
                break;
            case 6:
                estados.add(14);
                break;
        }
    }

    public static void trataT(List<Integer> estados) {
        switch (estados.get(estados.size() - 1)) {
            case 0:
            case 6:
                estados.add(2);
                break;

            case 8:
                estados.add(15);
                break;

            case 9:
                estados.add(16);
                break;

        }
    }

    public static void trataP(List<Integer> estados) {
        switch (estados.get(estados.size() - 1)) {
            case 0:

            case 6:

            case 8:
                estados.add(3);
                break;

            case 9:
                estados.add(13);
                break;

            case 10:
                estados.add(17);
                break;

            case 11:
                estados.add(18);
                break;

        }
    }

    public static void trataF(List<Integer> estados) {
        switch (estados.get(estados.size() - 1)) {
            case 0:

            case 11:

            case 10:

            case 9:

            case 8:

            case 6:
                estados.add(5);
                break;

            case 12:
                estados.add(19);
                break;

            case 13:
                estados.add(20);
                break;

        }
    }
}
