package br.com.gonleo;

public class Token {

    private int tipo;
    private String termo;

    public Token (int tipo, String termo){
        setTipo(tipo);
        setTermo(termo);
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getTermo() {
        return termo;
    }

    public void setTermo(String termo) {
        this.termo = termo;
    }

    @Override
    public String toString() {
        return "Token [" + tipo + ", " + termo + "]";
    }
}
