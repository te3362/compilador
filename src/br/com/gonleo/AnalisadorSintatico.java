package br.com.gonleo;

import br.com.gonleo.utils.SintaticoUtils;

import java.util.ArrayList;
import java.util.List;

public class AnalisadorSintatico {

    private List<String> simbolos = new ArrayList<String>();
    private List<Integer> estados = new ArrayList<Integer>();
    private boolean desvio = false;
    private boolean acc = false;

    public void analitico(List<Token> entrada) {
        estados.add(0);

        while (acc) {
            if (entrada.get(entrada.size() - 1).getTermo().equals("+")) {
                desvio = SintaticoUtils.trataSoma(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("-")) {
                desvio = SintaticoUtils.trataSub(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("*")) {
               desvio = SintaticoUtils.trataMult(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("/")) {
                desvio = SintaticoUtils.trataDiv(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("^")) {
                desvio = SintaticoUtils.trataPow(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("EXP")) {
                desvio = SintaticoUtils.trataExp(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("[")) {
                desvio = SintaticoUtils.trataColOpen(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("]")) {
                desvio = SintaticoUtils.trataColClose(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("(")) {
                desvio = SintaticoUtils.trataParOpen(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals(")")) {
                desvio = SintaticoUtils.trataParClose(simbolos, estados, entrada);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("id")) {
                desvio = SintaticoUtils.trataId(simbolos, estados, entrada);
            }

            if (entrada.get(entrada.size() - 1).getTermo().equals("$")) {
                if(estados.get(estados.get(estados.size() - 1)).intValue() == 1){
                    acc = true;
                }else{
                    desvio = SintaticoUtils.trataCifrao(simbolos, estados);
                }
            }

            if (entrada.get(entrada.size() - 1).getTermo().equals("E")) {
                SintaticoUtils.trataE(estados);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("T")) {
                SintaticoUtils.trataT(estados);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("P")) {
                SintaticoUtils.trataP(estados);
            }
            if (entrada.get(entrada.size() - 1).getTermo().equals("F")) {
                SintaticoUtils.trataF(estados);
            }
            if (desvio) {
                if (simbolos.get(simbolos.size() - 1).equals("E")) {
                    SintaticoUtils.trataE(estados);
                }
                if (simbolos.get(simbolos.size() - 1).equals("T")) {
                    SintaticoUtils.trataT(estados);
                }
                if (simbolos.get(simbolos.size() - 1).equals("P")) {
                    SintaticoUtils.trataP(estados);
                }
                if (simbolos.get(simbolos.size() - 1).equals("F")) {
                    SintaticoUtils.trataF(estados);
                }
            }
        }
    }
}