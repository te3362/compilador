package br.com.gonleo.enums;

public enum TokenTypeEnum {

    INTEIRO(0),
    LETRA(1),
    DECIMAL(2),
    CALCULO(3),
    PARAMETRO(4),
    ESPACO(5);

    private int numVal;

    TokenTypeEnum(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }
}
