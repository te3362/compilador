package br.com.gonleo;

import br.com.gonleo.enums.TokenTypeEnum;
import br.com.gonleo.utils.LexicoUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AnalisadorLexico {

    private int estado;
    private final String conteudo;
    private int posi;

    public AnalisadorLexico() throws IOException {

        // Lendo arquivo input.txt com as expressões matemáticas
        BufferedReader br = new BufferedReader(new FileReader("input.txt"));

        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = br.readLine();
        }
        conteudo = sb.toString();
    }

    public Token buscaToken() {
        if (isEOF()) {
            return null;
        }
        estado = 0;
        char c;
        Token token;
        StringBuilder termo = new StringBuilder();
        while (true) {
            c = nextChar();
            switch (estado) {
                case 0:
                    if (LexicoUtils.isEspaco(c)) {
                        estado = 10;
                    } else if (LexicoUtils.isParametro(c)) {
                        termo.append(c);
                        estado = 8;
                    } else if (LexicoUtils.isCalculo(c)) {
                        termo.append(c);
                        estado = 9;
                    } else if (LexicoUtils.isNumero(c)) {
                        termo.append(c);
                        estado = 1;
                    } else if (LexicoUtils.isLetra(c, 'E')) {
                        termo.append(c);
                        estado = 5;
                    } else {
                        tokenNaoReconhecido(c);
                    }
                    break;

                case 1:
                    if (LexicoUtils.isNumero(c)) {
                        termo.append(c);
                        estado = 1;
                    }
                    if (LexicoUtils.isPonto(c)) {
                        termo.append(c);
                        estado = 2;
                    } else {
                        estado = 4;
                    }
                    break;
                case 2:
                    if (LexicoUtils.isNumero(c)) {
                        termo.append(c);
                        estado = 3;
                    } else {
                        tokenNaoReconhecido(c);
                    }
                    break;

                case 3:
                    if (LexicoUtils.isNumero(c)) {
                        estado = 3;
                    } else {
                        token = new Token(TokenTypeEnum.DECIMAL.getNumVal(), termo.toString());
                        back();
                        return token;
                    }
                    break;

                case 4:
                    token = new Token(TokenTypeEnum.INTEIRO.getNumVal(), termo.toString());
                    back();
                    return token;

                case 5:
                    if (LexicoUtils.isLetra(c, 'X')) {
                        termo.append(c);
                        estado = 6;
                    } else {
                        tokenNaoReconhecido(c);
                    }
                    break;

                case 6:
                    if (LexicoUtils.isLetra(c, 'P')) {
                        termo.append(c);
                        estado = 7;
                    } else {
                        tokenNaoReconhecido(c);
                    }
                    break;

                case 7:
                    token = new Token(TokenTypeEnum.LETRA.getNumVal(), termo.toString());
                    back();
                    return token;

                case 8:
                    token = new Token(TokenTypeEnum.PARAMETRO.getNumVal(), termo.toString());
                    back();
                    return token;


                case 9:
                    token = new Token(TokenTypeEnum.CALCULO.getNumVal(), termo.toString());
                    back();
                    return token;

                case 10:
                    if (LexicoUtils.isEspaco(c)) {
                        estado = 10;
                    } else {
                        token = new Token(TokenTypeEnum.ESPACO.getNumVal(), "");
                        back();
                        return token;
                    }
                    break;

                default:
                    throw new IllegalStateException("Unexpected value: " + estado);
            }
        }
    }

    private void tokenNaoReconhecido(char c) {
        throw new RuntimeException("token: '" + c + "' não reconhecido");
    }

    private boolean isEOF() {
        return (posi == conteudo.toCharArray().length);
    }

    private char nextChar() {
        if (isEOF()) {
            return 0;
        }
        return conteudo.toCharArray()[posi++];
    }

    private void back() {
        if (!isEOF()) {
            posi--;
        }
    }
}